# Configuration for ubuntu 18.04

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y python3.7 build-essential
sudo apt-get install -y python3-pip

sudo pip3 install cookiecutter
sudo pip3 install --upgrade awscli
sudo pip3 install pipenv

mkdir -p ~/.local/bin/

wget -O ~/.local/bin/rmate https://raw.githubusercontent.com/aurora/rmate/master/rmate
chmod +x ~/.local/bin/rmate

# Make sure ~/.local/bin is in the path
[[ ":$PATH:" != *":${HOME}/.local/bin:"* ]] && echo "export PATH=${HOME}/.local/bin:${PATH}" >> ~/.bashrc

echo "export EDITOR=vim" >> ~/.bashrc
echo "export TERM=xterm-256color" >> ~/.bashrc

curl -o $HOME/.tmux.conf https://raw.githubusercontent.com/gpakosz/.tmux/master/.tmux.conf

git config --global user.name "Chris Cotter"
git config --global user.email "ccotter@bensonhillbio.com"\

# Install pyenv
sudo apt-get install make build-essential libssl-dev zlib1g-dev \
libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm \
libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

git clone https://github.com/pyenv/pyenv.git ~/.pyenv

export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"

eval "$(pyenv init -)"

# Update current console
source ~/.bashrc
